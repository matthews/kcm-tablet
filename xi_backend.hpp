#pragma once

#include "backend.hpp"
#include "xi_tablet.hpp"

#include <QGuiApplication>
#include <QObject>
#include <QProcess>
#include <QX11Info>
#include <X11/Xdefs.h>

class XiBackend : public Backend {
    Q_OBJECT

    static inline const char * const FALLBACK = "fallback";

    XiBackend(const std::unordered_map<std::string, xcb_atom_t>);

    template<typename T> std::vector<T> getProp(XID, Atom, Atom, int, unsigned long)  const;
    std::optional<std::pair<uint16_t, uint16_t>> get_vidpid(XID) const;
    QString XiErrName(int) const;

    Display * const dpy;
    const xcb_atom_t float_, xi_tablet, id, pad, pen,
                     calibrationMatrixDefault, leftHanded, leftHandedDefault;

public:
    // public so the kcm can set calibration matricies at login
    const xcb_atom_t calibrationMatrix;
    static std::unique_ptr<XiBackend> build();

    void defaults(Tablet&) const override;
    void load(Tablet&) const override;
    void save(Tablet&) const override;
    bool apply(XiTablet&) const;
};
