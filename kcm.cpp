#include "kcm.hpp"

#include "kwin_backend.hpp"

#include <KLocalizedString>
#include <KPluginFactory>
#include <KWindowSystem>
#include <QDateTime>
#include <QQuickItem>

#ifdef USE_X11
    #include "xi_backend.hpp"
    #include <QX11Info>
    #include <unistd.h>
    #include <X11/Xlib.h>
    #include <X11/Xlib-xcb.h>
    #include <xcb/xcb.h>
    // This include comes from xf86-input-libinput and needs its own check in cmake
    #include <xorg/libinput-properties.h>
#endif

K_PLUGIN_FACTORY_WITH_JSON(KCMTabletFactory, "kcm_tablet.json", registerPlugin<KCMTablet>();)

const std::unique_ptr<Backend> platform() {
    if (KWindowSystem::isPlatformWayland()) {
        return std::make_unique<KWinBackend>();
#ifdef USE_X11
    } else if (KWindowSystem::isPlatformX11()) {
            return XiBackend::build();
#endif
    } else {
        qCCritical(KCM_TABLET) << "No valid window system";
        return {};
    }
}

extern "C" {
    Q_DECL_EXPORT void kcminit_tablet() {
        auto backend = platform();
        if(KWindowSystem::isPlatformWayland())
            qCDebug(KCM_TABLET) << "Nothing happens on Kwin yet";
#ifdef USE_X11
        else if(KWindowSystem::isPlatformX11()) {
            auto& xbak = static_cast<XiBackend&>(*backend);
            for(auto tab : xbak.tablets()) {
                auto& xtab = static_cast<XiTablet&>(*tab);
                xbak.load(xtab);
                xbak.apply(xtab);

                auto args = QStringList() << QStringLiteral("set-prop")
                                          << QString::number(xtab.tablet)
                                          << QString::number(xbak.calibrationMatrix);
                // Fallback is only populated if something is unavailable to compute a fresh matrix
                QMatrix3x3 matrix = xtab.fallback.isIdentity() ? xtab.compute() : xtab.fallback;
                for(uchar i = 0; i < 3; ++i)
                    for(uchar j = 0; j < 3; ++j)
                        args << QString::number(matrix(i,j));
                qCDebug(KCM_TABLET) << "xinput invocation args: " << args;

                QProcess xinput;
                xinput.start(QStringLiteral("xinput"), args);
                xinput.waitForFinished();
                QByteArray res = xinput.readAll();
                if(!res.isEmpty())
                    qCCritical(KCM_TABLET) << "Setting calibration failed, return was: " << res;
            }
        }
#endif
        else
            qCCritical(KCM_TABLET) << "Init fail - no valid window system";
    }
}

KCMTablet::KCMTablet(QObject* parent, const QVariantList& args)
    : KQuickAddons::ConfigModule(parent, args),
      backend{platform()},
      m_current(nullptr),
      appName(qGuiApp->applicationDisplayName()) {
    // This DOESN'T leak, KCModule's destructor deletes it...
    auto about = new KAboutData(QStringLiteral("kcm_tablet"), i18n("Tablet"),  QStringLiteral("0.1"),
                                i18n("Pen tablet configuration"), KAboutLicense::GPL);
    about->addAuthor(QStringLiteral("zan"), QStringLiteral("Author"), QStringLiteral("zan@420blaze.it"));
    setAboutData(about);
    // TODO QML error page instead of fatal?
    if(!backend)
        qFatal("Backend initialization failed");
    else if(backend->tablets().empty()) {
        qCDebug(KCM_TABLET) << "No tablets present at startup";
    }
    else {
        setCurrent(backend->tablets().first());
    }

    connect(backend.get(), &Backend::added, this, [&](Tablet& tab) {
        if(!m_current)
            setCurrent(&tab);
    });
    connect(backend.get(), &Backend::removed, this, [&](Tablet& tab) {
        if(backend->tablets().empty()) {
            m_current = nullptr;
            emit currentChanged(nullptr);
        }
        else if(m_current == &tab)
            setCurrent(backend->tablets().first());
    });
    connect(backend.get(), &Backend::added, this, &KCMTablet::tabletsChanged);
    connect(backend.get(), &Backend::removed, this, &KCMTablet::tabletsChanged);
    // This is connected after SetCurrent so the initial tablet
    // isn't loaded because KCM startup will call load for us
    connect(this, &KCMTablet::currentChanged, this, [&]() { if(m_current && !m_current->loaded) load(); });

    backend->setParent(this);
    setButtons(Button::Default | Button::Apply);

    qmlRegisterUncreatableType<Calibration>("calibration", 1, 0, "Calibration", "Call kcm.calibration to make with its arg rect");
    qmlRegisterUncreatableType<Tablet>("tablet", 1, 0, "Tablet", "Only for the Rotation enum");

    if(KWindowSystem::isPlatformWayland())
        mainUi()->setProperty("wayland", true);
}

KCMTablet::~KCMTablet()
{
    qGuiApp->setApplicationDisplayName(appName);
}

QSize KCMTablet::area() const {
   return monitor.area();
}

Calibration* KCMTablet::calibration(QRect target) {
    auto calib = new Calibration(target);
    connect(calib, &Calibration::finalized, [this](CaliVars vars) {
        m_current->scaleCalibration(vars);
    });
    return calib;
}

void KCMTablet::defaults() {
    if(m_current)
        backend->defaults(*m_current);
}

void KCMTablet::load() {
    if(m_current)
        backend->load(*m_current);
    setNeedsSave(false);
}

void KCMTablet::save() {
    if(m_current)
        backend->save(*m_current);
    setNeedsSave(false);
}

Tablet* KCMTablet::current() const {
    return m_current;
}

QList<QObject*>& KCMTablet::tablets() const {
    return reinterpret_cast<QList<QObject*>&>(backend->tablets());
}

void KCMTablet::setCurrent(QObject* obj) {
    Tablet* tab = qobject_cast<Tablet*>(obj);
    if(tab && tab != m_current) {
        m_current = tab;

        qGuiApp->setApplicationDisplayName(m_current->name + " ― " + appName);
        // Do this before the connects since load can be called, only trustworthy
        // with a synchronous connection. Good thing its on the same object.
        emit currentChanged(m_current);
        // Can't do these in a loop since the signals have variable arguments
        // Technically changing tablets keeps these signals intact so if an inactive tablet were
        // to change something for some reason it would set the GUI needSave on the current tablet.
        connect(m_current, &Tablet::displayChanged, this, [this] { setNeedsSave(true); });
        connect(m_current, &Tablet::leftChanged, this, [this] { setNeedsSave(true); });
        connect(m_current, &Tablet::calibrationChanged, this, [this] { setNeedsSave(true); });
        connect(m_current, &Tablet::rotationChanged, this, [this] { setNeedsSave(true); });
        connect(m_current, &Tablet::buttonsChanged, this, [this] { setNeedsSave(true); });
        connect(m_current, &Tablet::buttonChanged, this, [this] { setNeedsSave(true); });
    }
}

#include "kcm.moc"
