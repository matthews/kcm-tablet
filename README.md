# tablet-kcm

KDE Control Module to manage pen tablets managed by libinput on X or Wayland.

# Todo

- X11 management daemon so settings are not only applied on boot
- KWin support for calibration and button mapping (and for buttons in general)
- KWin support for display assignment?
- Device profiles
- Auto-detect mapped screen for a display tablet (unreliable)

# License

[GPLv3](COPYING)
