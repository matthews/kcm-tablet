#include "calibration.hpp"

Calibration::Calibration::Calibration(QRect target) :
    target(target), complete(0) {
        for(Corner& c : corners)
            c = {0, 0 ,0};
}

void Calibration::accumulate() {
    qint64 ns = timer.nsecsElapsed();
    corners[m_corner].x += m_point.x() * ns;
    corners[m_corner].y += m_point.y() * ns;
    corners[m_corner].ns += ns;
}

void Calibration::start(Qt::Corner corner)
{
    m_corner = corner;
    timer.start();
}

void Calibration::clear(Qt::Corner corner) {
    corners[corner] = {0, 0, 0};
    timer.invalidate();
}

void Calibration::finish() {
    if(!timer.isValid()) qCDebug(KCM_TABLET) << "Callibration timer shouldn't be invalid calling finish";
    accumulate();
    timer.invalidate();
    if(++complete == corners.size())
        emit finalized(finalize());
}

void Calibration::commit(QPointF point) {
    if(timer.isValid()) {
        accumulate();
        timer.restart();
    }
    m_point = point;
}

CaliVars Calibration::finalize() const {
    const long double left = corners[TopLeft].x / (corners[TopLeft].ns * 2) +
                             corners[BottomLeft].x /  (corners[BottomLeft].ns * 2),
                      right = corners[TopRight].x / (corners[TopRight].ns * 2) +
                              corners[BottomRight].x / (corners[BottomRight].ns * 2),
                      top = corners[TopLeft].y / (corners[TopLeft].ns * 2) +
                            corners[TopRight].y / (corners[TopRight].ns * 2),
                      bottom = corners[BottomLeft].y / (corners[BottomLeft].ns * 2) +
                               corners[BottomRight].y / (corners[BottomRight].ns * 2),
    // Its actually documented that QRect.right is wrong on purpose??
                      a = target.width() / (right - left),
                      c = (target.x() - left * a) / target.width(),
                      e = target.height() / (bottom - top),
                      f = (target.y() - top * e) / target.height();
    return CaliVars{ a, c, e, f };
}
