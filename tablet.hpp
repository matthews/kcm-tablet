#pragma once

#include "calivars.hpp"

#include <QGenericMatrix>
#include <QList>
#include <QMap>
#include <QRectF>
#include <QScreen>

namespace {
typedef QGenericMatrix<3, 3, long double> Matrix;
}

static const Matrix REFLECTION { std::array<long double, 9> {-1, 0, 1, 0, -1, 1, 0, 0, 1 }.data() };
static inline const char DELIM = ';';

class Tablet : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER name CONSTANT)
    Q_PROPERTY(quint16 product MEMBER product CONSTANT)
    Q_PROPERTY(quint16 vendor MEMBER vendor CONSTANT)
    Q_PROPERTY(QScreen* display READ display WRITE setDisplay NOTIFY displayChanged)
    Q_PROPERTY(QScreen* activeDisplay READ activeDisplay NOTIFY activeDisplayChanged)
    Q_PROPERTY(QString displayName READ displayName WRITE findDisplay NOTIFY displayNameChanged)
    Q_PROPERTY(bool left READ left WRITE setLeft NOTIFY leftChanged)
    Q_PROPERTY(bool activeLeft READ activeLeft NOTIFY activeLeftChanged)
    Q_PROPERTY(bool nativeLeft MEMBER nativeLeft CONSTANT)
    Q_PROPERTY(Rotation rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(Rotation activeRotation READ activeRotation NOTIFY activeRotationChanged)
    Q_PROPERTY(QRectF calibration READ calibrationRect NOTIFY calibrationChanged)
    Q_PROPERTY(QList<uint> buttons READ buttons WRITE setButtons NOTIFY buttonsChanged)

public:
    // Internal high-resolution matrix data
    // We don't use Qt::ScreenOrientation because we rotate the QML icon by these values
    enum class Rotation {
        Zero = 0,
        Ninety = 90,
        OneEighty = 180,
        TwoSeventy = 270
    };
    Q_ENUM(Rotation)

    // Ugly hack to have this just be public but for backend to exclusively set this 
    // would require a cyclic dependency on friending it if it were private
    bool loaded;

    const QString name,
                  slug;
    const quint16 vendor,
                  product;
    const bool nativeLeft,
               defaultLeft;

    QScreen* display() const;
    QScreen* activeDisplay() const;
    QString displayName() const;
    bool left() const {
        return m_left;
    }
    bool activeLeft() const {
        return m_active_left;
    }
    Tablet::Rotation rotation() const {
        return m_rotation;
    }
    Tablet::Rotation activeRotation() const {
        return m_active_rotation;
    }
    const CaliVars& calibration() const {
        return m_calibration;
    }
    const CaliVars& activeCalibration() const {
        return m_active_calibration;
    }
    const QList<uint>& buttons() const {
        return m_buttons;
    }

    Matrix calibrationMatrix() const;
    // Convenience functions to generate matricies
    Matrix mapped() const;
    void rotate(Matrix &) const;

    void setDisplay(QScreen*);
    // This is only necessary because there is no way to access the QScreen wrapped
    // by a QQuickScreen. This assumes names are unique - if they aren't, we could
    // check model, manufacturer, and serial
    void findDisplay(const QString&);
    void setActiveDisplay();
    void setLeft(bool);
    void setActiveLeft();
    void setRotation(Rotation);
    void setActiveRotation();
    void setCalibration(const CaliVars&);
    void setActiveCalibration();
    void setButtons(QList<uint>);

    void setActive();

    void scaleCalibration(const CaliVars&);

    Q_INVOKABLE void toggleLeft();
    Q_INVOKABLE void setButton(int, uint);

signals:
    void displayChanged(const QScreen*);
    void activeDisplayChanged(const QScreen*);
    void displayNameChanged(const QString&);
    void leftChanged(bool);
    void activeLeftChanged(bool);
    void rotationChanged(Rotation);
    void activeRotationChanged(Rotation);
    void calibrationChanged(const QRectF&);
    void buttonsChanged(const QList<uint>&);
    void buttonChanged(const int, const uint);

protected:
    Tablet(QString, quint16, quint16, bool, bool, QObject * = nullptr);

    const QScreen *m_display, *m_acitve_display;
    bool m_left, m_active_left;
    CaliVars m_calibration, m_active_calibration;
    Rotation m_rotation, m_active_rotation;
    QList<uint> m_buttons;

    QRectF calibrationRect() const;
};
