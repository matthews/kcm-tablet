#pragma once

#include "tablet.hpp"

#include <KSharedConfig>

class Backend : public QObject {
    Q_OBJECT

public:
    Backend() : cfg(KSharedConfig::openConfig(QStringLiteral("kcmtabletrc"))) {};
    ~Backend() { qDeleteAll(m_tablets); }

    virtual void defaults(Tablet&) const = 0;
    virtual void load(Tablet&) const = 0;
    virtual void save(Tablet&) const = 0;

    QList<Tablet*>& tablets() { return m_tablets; }

signals:
    void added(Tablet&);
    void removed(Tablet&);

protected:
    static inline const char * const DISPLAY = "display";
    static inline const char * const LEFT =  "left";
    static inline const char * const CALIBRATION = "calibration";
    static inline const char * const ROTATION = "rotation";
    static inline const char * const BUTTONS = "buttons";

    QString slug(const QScreen*) const;
    QScreen* deslug(QString) const;

    const KSharedConfigPtr cfg;
    // This is gross I want smart pointers
    QList<Tablet*> m_tablets;
};
