#pragma once

#include <QGuiApplication>

class ScreenMonitor : public QObject {
    Q_OBJECT

    Q_PROPERTY(QSize area READ area NOTIFY areaChanged)

public:
    ScreenMonitor();

   static QSize& area() {
       if(m_area.isEmpty()) calc();
       return m_area;
    }
    static void calc();

signals:
    void areaChanged(QSize&);

public slots:
    void modified();

private:
    static QSize m_area;
};
