#pragma once

#include "tablet.hpp"

#include <X11/Xdefs.h>

class XiTablet : public Tablet {
    Q_OBJECT

public:
    XiTablet(QString, quint16, quint16, bool, bool, bool, XID, XID = 0, short = 0, QObject * = nullptr);

    static QMatrix3x3 downcast(const Matrix &);

    const XID tablet;
    const XID pad;
    const short num_buttons;
    bool btnmod;
    QMatrix3x3 fallback;
    std::unordered_map<int, XID> pens;

    QMatrix3x3 compute() const;
};
