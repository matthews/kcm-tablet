#pragma once

#include "backend.hpp"

#include <QDBusInterface>

class KWinBackend : public Backend {
    Q_OBJECT

    struct Orphan {
        QString sysName;
        quint16 vendor;
        quint16 product;
    };
    std::list<Orphan> orphans;

public:
    KWinBackend();

    void defaults(Tablet&) const override;
    void load(Tablet&) const override;
    void save(Tablet&) const override;

private slots:
    void add(const QString &);
    void remove(const QString &);

private:
    QDBusInterface kwin;
};
