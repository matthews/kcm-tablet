#include "kwin_tablet.hpp"

KWinTablet::KWinTablet(std::unique_ptr<QDBusInterface>&& bus, QString name, QString sysName, quint16 product, quint16 vendor,
                       bool nativeLeft, bool defaultLeft, QObject* parent):
                       Tablet(name, product, vendor, nativeLeft, defaultLeft, parent),
                       dbus(std::move(bus)), sysName(sysName) {
    dbus->setParent(this);
}
