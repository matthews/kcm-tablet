#include "screen_monitor.hpp"

#include <KWindowSystem>
#include <QScreen>

ScreenMonitor::ScreenMonitor() {
    if(auto app = qGuiApp) {
        connect(app, &QGuiApplication::screenAdded, this, &ScreenMonitor::modified);
        connect(app, &QGuiApplication::screenRemoved, this, &ScreenMonitor::modified);
    }
}

void ScreenMonitor::calc() {
    QSize res {0, 0};
    qreal dpr = 1;
    for(QScreen *scr : QGuiApplication::screens()) {
        QRect geo = scr->geometry();
        if(KWindowSystem::isPlatformX11())
            dpr = scr->devicePixelRatio();
        int x = geo.x() + dpr * geo.width();
        int y = geo.y() + dpr * geo.height();
        if(x > res.width())
            res.setWidth(x);
        if(y > res.height())
            res.setHeight(y);
    }
    m_area = res;
}

void ScreenMonitor::modified() {
    calc();
    emit(areaChanged(m_area));
}

QSize ScreenMonitor::m_area {};
