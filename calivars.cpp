#include "calivars.hpp"

#include <QStringList>
#include <string>

bool CaliVars::isIdentity() const {
    return a == 1 && c == 0 && e == a && f == c;
}

bool CaliVars::operator==(const CaliVars& rhs) const {
    return !operator!=(rhs);
}

bool CaliVars::operator!=(const CaliVars& rhs) const {
    return a != rhs.a || c != rhs.c || e != rhs.e || f != rhs.f;
}

CaliVars CaliVars::operator*(const CaliVars& rhs) const {
    return {a * rhs.a, c + rhs.c, e * rhs.e, f + rhs.f };
}

CaliVars & CaliVars::operator*=(const CaliVars& rhs) {
    a *= rhs.a;
    c += rhs.c;
    e *= rhs.e;
    f += rhs.f;

    return *this;
}

QString CaliVars::toString() const {
    return QString::fromStdString(std::to_string(a) + ',' +
                                  std::to_string(c) + ',' +
                                  std::to_string(e) + ',' +
                                  std::to_string(f));
}

CaliVars CaliVars::fromString(const QString& str) {
        auto splits = str.split(',');
        if(splits.length() != 4) {
            qCDebug(KCM_TABLET) << "Calibration store should have 4 floats:" << splits.length() << str;
            return {};
        }

        // Exceptions turned on for this file because stold can throw
        try {
#define ld(i) std::stold(splits[i].toStdString())
            return {ld(0), ld(1), ld(2), ld(3)};
#undef ld
        } catch(...) {
            qCDebug(KCM_TABLET) << "Malformed float in calibration store:" << splits;
            return {};
        }
}


