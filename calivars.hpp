#pragma once

#include <QLoggingCategory>
#include <QString>

Q_DECLARE_LOGGING_CATEGORY(KCM_TABLET)

struct CaliVars {
    long double a = 1, c = 0, e = 1, f = 0;

    bool isIdentity() const;

    bool operator==(const CaliVars&) const;
    bool operator!=(const CaliVars&) const;
    CaliVars operator*(const CaliVars&) const;
    CaliVars& operator*=(const CaliVars&);

    QString toString() const;
    static CaliVars fromString(const QString&);
};
