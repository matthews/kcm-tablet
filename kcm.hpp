#pragma once

#include "calibration.hpp"
#include "backend.hpp"
#include "screen_monitor.hpp"
#include "tablet.hpp"

#include <memory>

#include <KAboutData>
#include <KQuickAddons/ConfigModule>

#include <QList>
#include <QMetaType>

class KCMTablet : public KQuickAddons::ConfigModule {
    Q_OBJECT

    Q_PROPERTY(Tablet* current READ current WRITE setCurrent NOTIFY currentChanged USER true)
    Q_PROPERTY(QList<QObject*> tablets READ tablets NOTIFY tabletsChanged)

    const std::unique_ptr<Backend> backend;
    const ScreenMonitor monitor;
    Tablet *m_current;
    const QString appName;

public:
    explicit KCMTablet(QObject*, const QVariantList&);
    ~KCMTablet() override;

    // Delegates ScreenMonitors area of the virtual desktop so QML can access it
    Q_INVOKABLE QSize area() const;
    // Calibration constructor since QML doesn't support arg'd type constructors
    Q_INVOKABLE Calibration* calibration(QRect);

    Tablet* current() const;
    QList<QObject*>& tablets() const;
    void setCurrent(QObject*);

public slots:
    void defaults() override;
    void load() override;
    void save() override;

signals:
    void currentChanged(Tablet*);
    void tabletsChanged();
};
