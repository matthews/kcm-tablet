#pragma once

#include "calivars.hpp"

#include <QElapsedTimer>
#include <QPointF>
#include <QRectF>

class Calibration : public QObject {
    Q_OBJECT

    struct Corner {
        long double x, y;
        quint64 ns;
    };

    const QRectF target;
    std::array<Corner, 4> corners;
    uchar complete;
    QElapsedTimer timer;
    QPointF m_point;
    Qt::Corner m_corner;

    void accumulate();
    CaliVars finalize() const;

public:
    // Qt::Corner isn't exposed in QML, so its redefined here so it can be accessed as Calibration.Corner.
    enum QtCorner {
        TopLeft = Qt::Corner::TopLeftCorner,
        TopRight = Qt::Corner::TopRightCorner,
        BottomLeft = Qt::Corner::BottomLeftCorner,
        BottomRight = Qt::Corner::BottomRightCorner
    };
    Q_ENUM(QtCorner);

    Calibration(QRect);

    Q_INVOKABLE void start(Qt::Corner);
    Q_INVOKABLE void clear(Qt::Corner);
    Q_INVOKABLE void finish();
    Q_INVOKABLE void commit(QPointF);

signals:
    void finalized(CaliVars);
};
